<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Session;
use Inertia\Inertia;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // jika ada error, kirim ke session
        Inertia::share([
            'errors' => function(){
                return Session::get('errors') ? Session::get('errors') : (object) [];
            },
        ]);

        // jika ada message kirim ke session
        Inertia::share('flash', function() {
            return [
                'message' => Session::get('message'),
            ];
        });
    }
}
