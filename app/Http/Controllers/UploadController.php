<?php

namespace App\Http\Controllers;

use App\Models\UploadModel;
use Illuminate\Http\Request;
use Inertia\Inertia;
use File;

class UploadController extends Controller
{
    public function upload()
    {
        $data = UploadModel::all();

        return Inertia::render('Upload', ['data' => $data]);
    }

    public function doUpload(Request $request)
    {
        $request->validate([
            'images' => 'required',
        ]);

        if (preg_match("/png;base64/i", $request->images)) {
            $image     = $request->images;
            $image     = str_replace('data:image/png;base64,', '', $image);
            $image     = str_replace(' ', '+', $image);
            $imageName = '/images-upload/images' . '_' . date('hisdmY') . '.' . 'png';

        } else if (preg_match("/jpg;base64/i", $request->images)) {
            $image     = $request->images;
            $image     = str_replace('data:image/jpg;base64,', '', $image);
            $image     = str_replace(' ', '+', $image);
            $imageName = '/images-upload/images' . '_' . date('hisdmY') . '.' . 'jpg';

        } else if (preg_match("/jpeg;base64/i", $request->images)) {
            $image     = $request->images;
            $image     = str_replace('data:image/jpeg;base64,', '', $image);
            $image     = str_replace(' ', '+', $image);
            $imageName = '/images-upload/images' . '_' . date('hisdmY') . '.' . 'jpeg';
        }

        $create = UploadModel::create([
            'name' => $imageName,
        ]);

        File::put(public_path() . $imageName, base64_decode($image));

        return redirect()->back()->with('message', 'Images uploaded!');
    }
}
