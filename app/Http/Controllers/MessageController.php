<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Mail\MessageEmail;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    // render page for email
    public function emailPage(Request $request)
    {
    	return Inertia::render('Mail');
    }

    // send message via email
    public function sendEMail(Request $request)
    {
    	Mail::to($request->email_receiver)->send(new MessageEmail($request->message));

    	return redirect()->back()->with('message', 'Email to ' . $request->email_receiver . ' has been send!');
    }
}
