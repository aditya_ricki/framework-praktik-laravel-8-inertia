<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\MessageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/fug', function () {
    return 'Route fug';
});

Route::get('/fug/{param}', function ($param) {
    return 'Route /fug dengan parameter ' . $param;
});

// route ke dashboard
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia\Inertia::render('Dashboard');
})->name('dashboard');

// route group ke url yang diproteksi menggunakan middleware
Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::resource('categories', CategoryController::class);
    Route::get('/upload', [UploadController::class, 'upload'])->name('upload');
    Route::post('/upload', [UploadController::class, 'doUpload']);
    Route::get('/email-page', [MessageController::class, 'emailPage'])->name('email.page');
    Route::post('/send-email', [MessageController::class, 'sendEMail'])->name('send.email');
});
